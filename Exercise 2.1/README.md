# Exercise 2.1

## Usage with Docker
```bash
docker build -t deming_regression .
docker run deming_regression
```

## Explanation
Since in the exercise we have to use Deming regression with delta = 1, it becomes in an orthogonal regression in which the way we have to calculate errors changes in comparision to Ordinary Least Squares:

![picture](https://miro.medium.com/max/534/1*illoIj5LRD3NrQ69iV30kw.png)

So, I realized that SciPy (an open-source library) has the ODR (Orthogonal Distance Regression) package which contains some built-in functions that allow me to calculate the regression line that best fits the given data.

Also, I've made a little data analysis and found that in some observations it ocurrs that wind speed is > 0 but the production is 0. So I think that in those observations the generator is stopped for some reasons. So I've chosen to skip those values.

I've used Pandas library (open-source) to easily handle data with DataFrames.

## Jupyter Notebook
I've added a Jupyter notebook which contains some lines to plot results to validate the model.