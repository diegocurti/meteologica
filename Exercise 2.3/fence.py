import numpy as np
from scipy.spatial import ConvexHull, convex_hull_plot_2d

# Open input file with tree points
input_file = r'./input23.txt'

f = open(input_file, "r")

# Parse input as tuple of coordinates (x,y). 
# I choose convert it as float just in case it would be Lat-Lon coordinates
points = np.array([tuple(map(float, line.rstrip('\n').split(" "))) for line in f])

# Creates a Convex Hull
hull = ConvexHull(points)

# Print vertices
for x in hull.vertices:
    vertex = map(str, points[x])
    print(' '.join(vertex))