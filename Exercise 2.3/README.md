# Exercise 2.1

## Usage with Docker
```bash
docker build -t fence .
docker run fence
```

## Explanation
The first time I read the exercise I started thinking about how to get the minimum polygon that encloses all trees. But then I started to make some research and I found that it would be achieved with Convex Hull. Also, SciPy Spatial has a function that calculates the Convex Hull given some points. So, to avoid reinventing the wheel I decided to use this library.

For simplicity, the input is a text file with tree points.

## Jupyter Notebook
I've added a Jupyter notebook which contains some lines to plot results to validate the obtained polygon.