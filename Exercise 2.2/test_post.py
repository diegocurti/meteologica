import requests
import json


headers = {'Content-Type' : 'application/json'}
url = "http://127.0.0.1:5000/api/v1/predictions"
json = {
	"ubicacion": {
		"type": "Feature",
		"geometry": {
			"type": "Point",
			"coordinates": [125.6, 10.1]
		},
		"properties": {
			"name": "Dinagat Islands"
		}
	},
	"fecha": "2019-01-15"
}

r = requests.post(url, json=json, headers=headers)

print(r.json())