from flask import Flask, request, json, jsonify, Response
import numpy as np
import datetime

app = Flask(__name__)

# Define route for API predictions
@app.route('/api/v1/predictions', methods=['POST'])
def predictions():
    
    # Get JSON data from request
    json = request.json

    date_str = json['fecha']
    
    # Here I would get the city name to search predictions in DB.
    # Also, it depends on how predictions are stored but if they are calculated around a polygon 
    # and stored as GeoJSON in (let's say) a MongoDB Collections, we can perform a query to get 
    # those in which the given Point is inside this Polygon.
    # city = json['ubicacion']['properties']['name']
    
    # Convert string date as datetime object to operate easily with    
    date_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d')

    # It's been required 10 days of predictions
    DAYS_PRED = 10
    
    # Make predictions
    predictions = []
    for day in range(0, DAYS_PRED):
        # Add days to given date
        date_pred = date_obj + datetime.timedelta(days=day) 
        
        # Generate random temperature with one decimal place.
        # Since we are in summer, I choose to restrict range from 20 to 38 degrees
        tMax = round(np.random.uniform(20,38), 1)

        # Store in array
        predictions.append({
            "fecha": date_pred.strftime('%Y-%m-%d'),
            "TMax": tMax
        })

    # Create JSON response 
    resp = {
        "ubicacion": json["ubicacion"], 
        "prediccion": predictions
    }

    return jsonify(resp)


if __name__ == '__main__':
    # host='0.0.0.0' to be reachable in Docker container
    app.run(host='0.0.0.0', debug=True)
    