# Exercise 2.2

## Usage with Docker
```bash
docker build -t api_predictions .
docker run -p 5000:5000 api_predictions
```

## Explanation
I've chosen to use Flask (open-source) as the framework to define the API Rest because I used it before and I think it's simple to define routes.

I simply get JSON request and generate random temperature for next 10 days. As I've commented in the script, in a real scenario we can get the city name and search predictions in some DB or (depends on how data is stored) we can use the given GeoJSON position and make some GeoSpatial calculations, if needed.